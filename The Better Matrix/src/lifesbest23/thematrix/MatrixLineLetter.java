package lifesbest23.thematrix;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Random;

public class MatrixLineLetter {
	
	public int y;
	private char value;
	private Color color;
	private Random r = new Random();
	private BufferedImage mask = null;
	private boolean coloring = true;
	
	public MatrixLineLetter(){
		y = 0;
		
		//getting a random Letter
		
		int result = r.nextInt('z');
		if(result < 32)
			value = ' ';
		else
			value = (char) result;
		
		//getting a random Green Color
		int greenCode = r.nextInt(126);
		int alpha = r.nextInt(126);
		int rgb = 0x70_00_70_00;
		rgb += greenCode * 0xFF; //adding green value
		rgb += alpha * 0xFF_FF_FF;	//adding alpha value
		
		color = new Color(rgb, true);
	}
	
	public void setMask(BufferedImage msk){
		mask = msk;
	}
	
	private void updateColor(){
		//getting a random Green Color
		int greenCode = r.nextInt(126);
		int alpha = r.nextInt(126);
		int rgb = 0x70_00_70_00;
		rgb += greenCode * 0xFF; //adding green value
		rgb += alpha * 0xFF_FF_FF;	//adding alpha value
		
		color = new Color(rgb, true);
	}
	
	private void setRandomRedColor(){
		if(y % 4 == 0){
			coloring = false;
			int redCode = r.nextInt(126);
			int alpha = r.nextInt(50);
			
			color = new Color(redCode + 150, 0, 0, alpha + 150);
		}
	}
	
	private void updateLetter(){
		//getting a random Letter
		
		int result = r.nextInt('z');
		if(result < 32)
			value = ' ';
		else
			value = (char) result;
	}
	
	public void update(){
		y++;
		
		if(y % 4 == 0 && coloring){
			updateColor();
		}
		if(y % 40 == 0)
			if(r.nextInt(100) > 80)
				updateLetter();
		
	}
	
	public void paint(Graphics g, int x){
		if(mask != null){
			try{
				if(mask.getRGB(x, y) == 0xff_00_00_00)
					return;
				else if(mask.getRGB(x, y) == 0xff_ff_00_00)
					setRandomRedColor();
				else
					coloring = true;
			}catch(Exception e){}
		}
		g.setColor(color);
		g.drawString(Character.toString(value), x, y);
	}
}
