package lifesbest23.thematrix;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class MatrixLine {
	private List<MatrixLineLetter> letters = new ArrayList<MatrixLineLetter>();
	private boolean run = true;
	private BufferedImage mask = null;
	
	public MatrixLine(){
		letters.add(new MatrixLineLetter());
	}
	public void run(){
		if(!run)
			return;
		try{
			if(letters.get(letters.size() - 1).y == 20){
				letters.add(new MatrixLineLetter());
				letters.get(letters.size() - 1).setMask(mask);
			}
			for(int i = 0; i < letters.size(); i++)
				letters.get(i).update();
		} catch(Exception e){}
	}
	
	public void setRun( boolean b){
		run = b;
	}
	
	public void setMask(BufferedImage im){
//		for(int i = letters.size() - 1; i >= 0; i--){
//			letters.get(i).setMask(im);
//		}
		mask = im;
	}
	
	public void draw(Graphics g, int x){
		for(int i = letters.size() - 1; i >= 0; i--){
			try{
				if( letters.get(i).y > g.getClipBounds().height )
					letters.remove(i++);
				else
					letters.get(i).paint(g, x);
			} catch (Exception e){}
		}
	}
}
