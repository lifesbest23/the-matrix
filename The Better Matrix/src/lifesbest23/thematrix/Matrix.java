package lifesbest23.thematrix;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Matrix extends JPanel implements ComponentListener, KeyListener{
	private static final long serialVersionUID = -7378782149517304451L;
	
	private List<MatrixLine> lines = new ArrayList<MatrixLine>();
	
	private int speed = 11;
	private boolean run = true;
	
	private String input = "";

	public Matrix(){
		super();
		this.addComponentListener(this);
		this.addKeyListener(this);
		for(int i = 0; i < 120; i++)
			lines.add(new MatrixLine());
		
		new Thread(){
			public void run(){
				while(true){
					if(!run)
						continue;
					for(int i = 0; i < lines.size() && i*20 < getWidth(); i++)
						lines.get(i).run();
					repaint();
					try {
						Thread.sleep(speed);
					} catch (InterruptedException e) {e.printStackTrace();}
				}
			}
		}.start();
	}
	
	public void setMask(BufferedImage im){
		for(int i = 0; i < lines.size(); i++)
			lines.get(i).setMask(im);
	}
	
	public void paintComponent(Graphics g){
		g.setColor(new Color(0x30_00_00_00, true));
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		for(int i = 0; i < lines.size() && i*20 < this.getWidth(); i++){
			lines.get(i).draw(g, i*20);
		}
	}

	@Override public void componentHidden(ComponentEvent arg0) {} 

	@Override public void componentMoved(ComponentEvent arg0) {}

	@Override public void componentResized(ComponentEvent e) {
		this.setPreferredSize(e.getComponent().getSize());
		this.revalidate();
	}

	@Override public void componentShown(ComponentEvent arg0) {}

	@Override public void keyPressed(KeyEvent e) {
		input += Character.toString(e.getKeyChar());
		if(input.toLowerCase().endsWith("there is no spoon")){
			try {
				this.setMask( ImageIO.read(getClass().getResource("/noSpoon2.png")));
			} catch (IOException e1) {e1.printStackTrace();}
		}
		else if(input.toLowerCase().endsWith("new sunrise")){
			System.exit(0);
		}
		else if(input.toLowerCase().endsWith("is there a spoon")){
			try {
				this.setMask( ImageIO.read(getClass().getResource("/spoon2.png")));
			} catch (IOException e1) {e1.printStackTrace();}
		}
		else if(input.toLowerCase().endsWith("there is really no spoon")){
			this.setMask( null );
		}
		else if(input.toLowerCase().endsWith("the matrix")){
			try {
				this.setMask( ImageIO.read(getClass().getResource("/matrix.png")));
			} catch (IOException e1) {e1.printStackTrace();}
		}
		else if(input.toLowerCase().endsWith("no matrix")){
			this.setMask( null );
		}
		else if(input.toLowerCase().endsWith("faster")){
			if(speed > 5)
				speed -= 5;
		}
		else if(input.toLowerCase().endsWith("slower")){
			if(speed < 50)
				speed += 5;
		}
		else if(input.toLowerCase().endsWith("lock")){
			run = false;
		}
		else if(input.toLowerCase().endsWith("key")){
			run = true;
		}
	}

	@Override public void keyReleased(KeyEvent arg0) {}

	@Override public void keyTyped(KeyEvent arg0) {}
}
