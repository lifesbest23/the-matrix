package lifesbest23.thematrix;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import javax.swing.JFrame;

public class Main extends JFrame{
	private static final long serialVersionUID = 1137086838153434556L;

	public Main(){
		super();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Matrix");
		
		Matrix m = new Matrix();
		this.add(m);
		this.addKeyListener(m);
		
		this.setSize(600, 600);

		this.setUndecorated(true);
		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice dev = env.getDefaultScreenDevice();
		this.pack();
		dev.setFullScreenWindow(this);
	}
	
	public static void main(String[] str){
		new Main();
	}

}
